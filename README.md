# Portfolio

A landing page redirecting to Valentin Le Gal's main networks.  

The website is currently online at the following address: https://www.valentinlegal.fr

![Website screenshot](./src/assets/img/screenshot.png)

## Setup

```
git clone git@gitlab.com:ValentinLeGal/portfolio.git
cd portfolio
npm install
npm run dev
```

## Deployment

```sh
npm run lint
npm run build
```

## Changelog

All notable changes to this project will be documented in the [CHANGELOG file](CHANGELOG.md).

## License

Released under the [MIT license](LICENSE).
