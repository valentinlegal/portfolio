# Change Log

All notable changes to this project will be documented in this file.

## [v1.1.0] - 2021-11-05

### Added

- Added a sitemap file
- Added the website's link and screenshot in the README file

### Changed

### Fixed

## [v1.0.3] - 2021-11-03

### Added

- Added robots.txt file

### Changed

### Fixed

## [v1.0.2] - 2021-11-03

### Added

### Changed

### Fixed

- Preloaded fonts and removed comments from builded HTML

## [v1.0.1] - 2021-11-02

### Added

### Changed

### Fixed

- Removed Google Analytics

## [v1.0.0] - 2021-11-02

### Added

- Integrated all website

### Changed

### Fixed
